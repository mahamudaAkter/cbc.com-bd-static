<?php 
  include('header.php');
?>
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">About Us</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About us <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-5 p-md-5 img img-2 mt-5 mt-md-0" style="background-image: url(images/bg_2.jpg);">
					</div>
					<div class="col-md-7 wrap-about py-5 px-4 px-md-5 ftco-animate">
	          <div class="heading-section mb-5">
	            <h2 class="mb-4">We Are Highly Recommendable Construction Firm</h2>
	          </div>
	          <div class="">
							<p class="text-justify">The Chittagong Builders Corporation (CBC) is a famous and renowned
                Construction company in Bangladesh. The organization established in
                the year 1990 considering its aim & objects as follows. We have been
                working successfully with good reputation dialed with more than 200
                companies in Chattogram EPZ, KEPZ, Korean EPZ and other government,
                semi-government, autonomous etc. factories and organization in
                industrial sector.<br/><br/> Our goal to expand our services widely to the industrial
                sector in Bangladesh with modern effective evaluation.
                We are the authorized contractor of BEPZA.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
    
   <?php 
    include('footer.php');
  ?>