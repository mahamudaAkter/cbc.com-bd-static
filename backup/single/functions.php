<?php 

function cbc_function(){
	//adding navwalker file 
	require_once get_template_directory() . '/wp-bootstrap-navwalker.php';

	//custom title 
	add_theme_support('title-tag');

	//thumbnail 
	add_theme_support('post-thumbnails');
	
	//custom background 
	add_theme_support('custom-background');
	
	//custom logo
	add_theme_support('custom-logo',array(
		'default-image' => get_template_directory_uri().'/images/cbc-logo.png'
	));

	add_theme_support('custom-logo',array(
		'default-image' => get_template_directory_uri().'/images/cbc-logo.png',
	));

	//load theme textdomain
	load_theme_textdomain('cbc', get_template_directory_uri().'/languages');

	//register menus
		if(function_exists('register_nav_menus')){
			register_nav_menus(array(
				'primarymenu' 		=> __('Header Menu','cbc'),
				'secondarymenu' 	=> __('Footer Menu','cbc')
			));
		}

	//read more
		function read_more($limit){
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content, 0, $limit);

		echo implode(" ", $less_content);
	}



}
add_action('after_setup_theme','cbc_function'); 







?>